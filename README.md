# i3 Config Files

## Install

    $ bash ~/.i3/install.sh

### Programs to Install (pamac & AUR)

    * android-studio
    * arduino
    * google-chrome
    * intellij
    * spotify
    * unity-editor
    * visual-studio-code

    * gnome-calculator
    * pcloudcc
    * pcloud
    * dropbox
    * transmission-gtk

    * materia-gtk-theme
    * papirus-maia-icon-theme
    * gcolor2

    * openvpn
    * kdeconnect
    * twmn-git
    * nmcli
    * gparted

### FontAwesome
[Download](https://github.com/FortAwesome/Font-Awesome), then move "Font Awesome 5 Free-Regular-400.otf" to ~/.fonts/

[Reference](https://fontawesome.com/cheatsheet?from=io)
